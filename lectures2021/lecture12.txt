- intel opt guide
- agner fog opt guide
- top-down perf analysis papre and references (also in appx A of intel opt guide)
- cppcon videos for performance
- scylladb perf blog pos
- pprof page
- pmu-tools
- godbold video for x86 hacking (maybe branch predictor posts either)
- samoilov prof for locs


Slides:
Code is slow, what could happen?
- HW, kernel /disk, net, memory allocation, etc/ (trance syscalls, use bpftrace for further investigation)
- Scheduling (lower number of tools, schedvis is one of them)
- lock blocking
- cpu code unoptimal: cache misses, cache hazards, pipeline stalls

Primer:
- opt memcpy from guide 

- Division example: idiv takes 60 cycles, not pipelined!. Use

Perf
- Events
- Counting
- Samplint

Perf usage
- stat
- record (sampling rate / frequency)
- report/annotate
- top

Primer
- matrix mul
- chart
- counters difference

Microarchitecture


Top-Down


MCA




SSE:
- computational model (vectors), motivation
- cpu flags support
- how to read mnemonics: p/s, s/d,i
- categories: arihmetic, shuffle, horizontal add/sub, set, extract, load, store, gather, scatter

- https://www.cs.virginia.edu/~cr4bd/3330/F2018/simdref.html
- https://www.intel.com/content/www/us/en/docs/intrinsics-guide/index.html

Google hash table
uint8_t hash8
hash8_packed = _mm_set1_epi8(hash8) // boadcast ui8 to all 16 slots of xmm
matches_packed = _mm_cmpeq_epi8(match, control) // stores per slot 0xff if match else 0 
uint32_t mask = _mm_movemask_epi8(matches_packed) // mask.bits()[i] = matches_packed[i].bits()[7]

- google hash table
- isa-l erasure coding
- bellman-ford


Links
Perf tutorial http://sandsoftwaresound.net/perf/perf-tutorial-hot-spots/
Official Perf tutorial: https://perf.wiki.kernel.org/index.php/Tutorial
Perf Examples by Brendan Gregg https://brendangregg.com/perf.html
Perf stat demo https://www.youtube.com/watch?v=2EWejmkKlxs

x86 microarchitecture overview by Matt Godbolt https://www.youtube.com/watch?v=hgcNM-6wr34
Intel® 64 and IA-32 Architectures Optimization Reference Manual https://www.intel.com/content/dam/www/public/us/en/documents/manuals/64-ia-32-architectures-optimization-manual.pdf
Agner Fog's Optimization Guides https://agner.org/optimize/
LLVM MCA https://llvm.org/docs/CommandGuide/llvm-mca.html
A. Abel and J. Reineke. uops.info: Characterizing Latency, Throughput, and Port Usage of Instructions on Intel Microarchitectures. ASPLOS 2019.

Ahmad Yasin. A Top-Down Method for Performance Analysis and Counters Architecture. ISPASS 2014
Toplev https://github.com/andikleen/pmu-tools/wiki/toplev-manual
ScyllaDB Top-Down analysis case https://www.scylladb.com/2017/07/06/scyllas-approach-improve-performance-cpu-bound-workloads/

Other Performance Links
PProf https://github.com/google/pprof
IPC measurements by Brendan Gregg https://www.brendangregg.com/blog/2017-05-04/the-pmcs-of-ec2.html
Casual Profiling methodology for identifying bottlenecks https://www.youtube.com/watch?v=r-TLSBdHe1A https://github.com/plasma-umass/coz
Linux Performance Tools by Brendan Gregg https://www.brendangregg.com/linuxperf.html
Schedviz https://opensource.googleblog.com/2019/10/understanding-scheduling-behavior-with.html
Branch target buffer reverse engeneered https://xania.org/201602/bpu-part-one


Simple C++ rules for performance https://www.youtube.com/watch?v=uzF4u9KgUWI

